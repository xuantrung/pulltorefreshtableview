//
//  ViewController.swift
//  drapTableView
//
//  Created by Admin on 3/6/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var arr : [Int] = []
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "loading")
        refreshControl.addTarget(self, action: #selector(self.scrollViewDidScroll), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.yellow
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if #available(iOS 10.0, *){
            self.tableView.refreshControl = refresher
        } else{
            tableView.addSubview(refresher)
        }
    }
    
    @objc func appendData(){
        for i in 1...1000{
            arr.append(i)
        }
        DispatchQueue.main.async {
            self.refresher.beginRefreshing()
            self.tableView.reloadData()
            self.refresher.endRefreshing()
        }
    }
    
    var canRefresh = true
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 0 {
            self.canRefresh = true
        }
        if scrollView.contentOffset.y == -100 { //change 100 to whatever you want
            if canRefresh && !self.refresher.isRefreshing {
                self.canRefresh = false
                self.appendData()
            }
        }
        if scrollView.contentOffset.y < -100 {
            scrollView.contentOffset.y = -100
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
        cell.textLabel?.text = String(arr[indexPath.row])
        return cell
    }
}
